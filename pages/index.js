import Head from 'next/head';
import Banner from '../components/Banner';

import {Container} from 'react-bootstrap';

export default function Home() {

  const data = {
    title: "Welcome to Nxt Budget Tracker!",
  }

  return (
    <React.Fragment>
      <Container>
        <Banner dataProp={data}/>
      </Container>
    </React.Fragment>
  )
}
