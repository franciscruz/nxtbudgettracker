import {useState, useEffect, useContext} from 'react';
import {Form, Button, Container, Jumbotron, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function addRecord() {

    const [categoryType, setCategoryType] = useState('income')
    const [categoryName, setCategoryName] = useState('')
    const [amount, setAmount] = useState('')
    const [description, setDescription] = useState('')
    const [categoriesInRecord, setCategoriesInRecord] = useState('')
    const {token} = useContext(UserContext)

    function record(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/add-record`, {
            method: 'POST',
            headers: {
            	Authorization: `Bearer ${localStorage.getItem('token')}`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
            	categoryType: categoryType,
            	categoryName: categoryName,
				amount: amount,
				description: description
	            })
	        })
            .then(res => {
                return res.json()
            })
            .then(data => {
                if(data === true){
                    Swal.fire('Record added successfully!', '', 'success');
                }else{
                    Swal.fire('Incomplete', '', 'warning');
                }
            })
    	}

     useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-categories`,{
            method: 'POST',
            headers: {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
             }
            })
        .then(res => res.json())
        .then(data => {
            const catNameArray = data.filter(category => {
                if(categoryType === 'income'){
                return(
                    category.type === 'income'     
                )
                }else{
                return(
                    category.type === 'expense'
                )
                } 
            })

            const newArray = catNameArray.map(categoriesRecord =>{
                return(<option key={categoriesRecord._id} value={categoriesRecord.name}>
                   {categoriesRecord.name}
                </option>)
            })
            setCategoryName(catNameArray[0].name)
            setCategoriesInRecord(newArray)
        })
    }, [token, categoryType])

	return (
            <Container>
                <Form onSubmit={(e) => record(e)}>
                    <Form.Label className="mt-2">Category Type</Form.Label>
                     <Form.Control controlId="categoryType" as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} required>
                         <option value='income'>Income</option>
                         <option value='expense'>Expense</option>
                     </Form.Control>
                     <Form.Label className="mt-2">Category Name</Form.Label>
                     <Form.Control controlId="categoryName" as="select" value={categoryName} onChange={e => setCategoryName(e.target.value)} required>
                         {categoriesInRecord}
                     </Form.Control>
                    <Form.Group className="mt-2" controlId="amount">
                        <Form.Label>Amount</Form.Label>
                        <Form.Control type="amount" placeholder="&#8369;" value={amount} onChange={e => setAmount(e.target.value)} required/>
                    </Form.Group>
                    <Form.Group className="mt-2" controlId="description">
                        <Form.Label>Record Description</Form.Label>
                        <Form.Control type="description" placeholder="Description" value={description} onChange={e => setDescription(e.target.value)} required/>
                    </Form.Group>
                    <Button className="float-right" variant="primary" type="submit" id="submitBtn">Submit</Button>
                  
                    </Form>
            </Container>
    		)
}