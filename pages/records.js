import {useState, useEffect} from 'react';
import {Form, Container, Jumbotron, Table} from 'react-bootstrap';

export default function getRecords(){

    const [records, setRecords] = useState('')
    const [isActive, setIsActive] = useState(false)
  
  useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-records`,{
            method: 'POST',
            headers: {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
             }
            })
        .then(res => res.json())
        .then(data => {
            let number=0;
            const newArray = data.map(newrecords =>{
                number++;
                return(<tr key={newrecords._id}>
                    <td>{number}</td>
                    <td>{newrecords.description}</td>
                    <td>{newrecords.categoryName}</td>
                    <td>{newrecords.categoryType}</td>
                    <td>{newrecords.amount}</td>
                    <td>{newrecords.balanceAfterTransaction}</td>
                    <td>{newrecords.dateAdded}</td>
                </tr>)
            })
            setRecords(newArray)
            })
    }, [])

    return (
        <Container>
            <Form>
                 <Form.Group>
                    <Jumbotron>
                    <Form.Label><h3>Records List</h3></Form.Label>
                            <Table className="fontsize" striped bordered hover variant="dark">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Record Description</th>
                                    <th>Category Name</th>
                                    <th>Category Type</th>
                                    <th>Amount</th>
                                    <th>Balance After Transaction</th>
                                    <th>Date Added</th>
                                </tr>
                                </thead>
                                <tbody>{records}</tbody>
                            </Table>
                    </Jumbotron>
                 </Form.Group>
            </Form>
        </Container>
    )
}
