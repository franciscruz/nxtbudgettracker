import {useState, useEffect} from 'react';
import { Form, Button, Container } from 'react-bootstrap';
import Router from 'next/router';

export default function index() {

    const [firstName, setfirstName] = useState('')
    const [lastName, setlastName] = useState('')
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    const [mobileNo, setmobileNo] = useState('')
    //state to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false)

    useEffect(() => {
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false)
        }
    }, [email, password1, password2])

    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/email-exists`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            //if no existing email is found
            if(data === false){
                fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/`, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    })
                })
                .then(res => {
                    return res.json()
                })
                .then(data => {
                    if(data === true){
                        alert("Registered successfully")
                        Router.push('/login')
                    }else{
                        alert("Registration failed")
                    }
                })
            }
        })
    }

    return (
        <Container>
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="firstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control type="firstName" placeholder="firstName" value={firstName} onChange={e => setfirstName(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="lastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control type="lastName" placeholder="lastName" value={lastName} onChange={e => setlastName(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="email">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="email" value={email} onChange={e => setEmail(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="mobileNo">
                    <Form.Label>Mobile Number</Form.Label>
                    <Form.Control type="mobileNo" placeholder="mobileNo" value={mobileNo} onChange={e => setmobileNo(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
                </Form.Group>
                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control type="password" placeholder="Verify Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
                </Form.Group>

                {isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">Submit</Button>
                    :
                    <Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
                }
            </Form>
        </Container>
    )
}