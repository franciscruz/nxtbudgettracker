import {useState, useEffect} from 'react';
import {Form, Button, Container, Jumbotron, Table} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function addCategory(){

	const [categoryType, setCategoryType] = useState('income')
    const [categoryName, setCategoryName] = useState('')
    const [categoryList, setCategoryList] = useState('')
    const [categories, setCategories] = useState('')
    const [isActive, setIsActive] = useState(false)

    function category(e){
        e.preventDefault();
        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/add-category`, {
            method: 'POST',
            headers: {
            	 Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                typeName: categoryType,
                name: categoryName
            })
        })
        .then(res => res.json())
        .then(data => {
        	if(data === true){
                Swal.fire('Category added successfully!', '', 'success');
            }else{
                Swal.fire('Incomplete', '', 'warning');
            }
        })
    }
  
  useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/get-categories`,{
            method: 'POST',
            headers: {
                 Authorization: `Bearer ${localStorage.getItem('token')}`
             }
            })
        .then(res => res.json())
        .then(data => {
            let number=0;
            const newArray = data.map(category =>{
                number++;
                return(<tr key={category._id}>
                    <td>{number}</td>
                    <td>{category.name}</td>
                    <td>{category.type}</td>
                </tr>)
            })
            setCategories(newArray)
            })
    }, [])

	return (
        <Container>
            <Form onSubmit={(e) => category(e)}>
                 <Form.Label className="mt-3">Category Type</Form.Label>
                 <Form.Control controlId="categoryType" as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)} required>
                     <option value='income'>Income</option>
                     <option value='expense'>Expense</option>
                 </Form.Control>
                 <Form.Group controlId="categoryName">
                    <Form.Label className="mt-3">Category Name</Form.Label>
                    <Form.Control type="categoryName" placeholder="Category Name" value={categoryName} onChange={e => setCategoryName(e.target.value)} required/>
                 </Form.Group>
                 <Button className="float-right" variant="primary" type="submit" id="submitBtn">Submit</Button>
                 <Form.Group controlId="categoryList">
                    <Jumbotron type="categoryList" value={categoryList} onChange={e => setCategoryList(e.target.value)}>
                    <Form.Label><h3>Category List</h3></Form.Label>
                            <Table striped bordered hover variant="dark">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Category Name</th>
                                    <th>Category Type</th>
                                </tr>
                                </thead>
                                <tbody>{categories}</tbody>
                            </Table>
                    </Jumbotron>
                 </Form.Group>
            </Form>
        </Container>
    )
}
