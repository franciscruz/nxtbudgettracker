import { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap'
import { GoogleLogin } from 'react-google-login'
import Router from 'next/router'
import Swal from 'sweetalert2'

import UserContext from '../UserContext';
import AppHelper from '../app-helper'
import View from '../components/View'

export default function index() {
    return (
        //title is the Prop 
        <View title={ 'Login' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Login</h3>
                    <LoginForm/>
                </Col>
            </Row>
        </View>
    )
}

const LoginForm = () => {
    const { token, setToken } = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [tokenId, setTokenId] = useState(null)

    const authenticate = (e) => {
        e.preventDefault()

        const options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'  },
            body: JSON.stringify({ email: email, password: password })
        }
        
        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/login`, options).then(AppHelper.toJSON).then(data => {
            if (typeof data.access !== 'undefined') {
                localStorage.setItem('token', data.access)
                setToken(data.access)
                Router.push('/addRecord')
            } else { //error messages
                if (data.error === 'does-not-exist') {
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password') {
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    const authenticateGoogleToken = (response) => {
        console.log(response)

        setTokenId(response.tokenId)

        const payload = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${process.env.NEXT_PUBLIC_API_BASE_URI}api/users/verify-google-id-token`, payload).then(AppHelper.toJSON).then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                setToken(data.accessToken)
                Router.push('/addRecord')
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire('Google Auth Error', 'Google authentication procedure failed, try again or contact web admin.', 'error')
                } else if (data.error === 'login-type-error') {
                    Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try alternative login procedures.', 'error')
                }
            }
        })
    }

    return (
        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={ authenticate }>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email Address</Form.Label>
                        <Form.Control type="email" value={ email } onChange={ (e) => setEmail(e.target.value) } autoComplete="off" required/>
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" value={ password } onChange={ (e) => setPassword(e.target.value) } required/>
                    </Form.Group>
                    <Button className="mb-2" variant="success" type="submit" block>Login</Button>
                    <GoogleLogin
                        clientId="517589865627-992sqdhs3v11qvpqcq5hekntf2jblm90.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={ authenticateGoogleToken }
                        onFailure={ authenticateGoogleToken }
                        cookiePolicy={ 'single_host_origin' }
                        className="w-100 text-center d-flex justify-content-center"
                    />
                </Form>
            </Card.Body>
        </Card>
    )
}