import {Doughnut} from 'react-chartjs-2'

//{} is for props, destructured
export default function DoughnutChart({income, expense}){
	return (
		<Doughnut data={{
			datasets: [{
				data: [income, expense],
				backgroundColor: ["blue", "red"]
			}],
			labels: ["Income", "Expense"]	
		}} redraw={false}/>
		)
}