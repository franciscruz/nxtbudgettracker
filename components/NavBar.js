import {useContext} from 'react';
import {Navbar, Nav, Image} from 'react-bootstrap';
import Link from 'next/link';
import UserContext from '../UserContext';

export default function NavBar(){
    const {token} = useContext(UserContext);

    return(
        <Navbar bg="light" expand="lg">
            <Image src="nxt.png" thumbnail />
            <Link href="/">
                <a className="navbar-brand ml-2">Budget Tracker</a>
            </Link>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            {(token !== null)
                    ?<React.Fragment>
                        <Nav className="mr-auto">
                            <Link href="/addCategory">
                            <a className="nav-link" role="button">Categories</a>
                            </Link>
                            <Link href="/records">
                            <a className="nav-link" role="button">Records</a>
                            </Link>
                            <Link href="/trend">
                            <a className="nav-link" role="button">Trend</a>
                            </Link>
                            <Link href="/breakdown">
                            <a className="nav-link" role="button">Breakdown</a>
                            </Link>
                            <Link href="/logout">
                            <a className="nav-link" role="button">Logout</a>
                            </Link>
                        </Nav>
                     </React.Fragment>
                    :<React.Fragment>
                            <Link href="/login">
                                <a className="nav-link" role="button">Login</a>
                            </Link>
                            <Link href="/register">
                                <a className="nav-link" role="button">Register</a>
                            </Link>
                        </React.Fragment>
                    } 
            </Navbar.Collapse>
        </Navbar>
    )
}